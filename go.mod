module modernc.org/gc

go 1.16

require (
	github.com/edsrzf/mmap-go v1.1.0
	modernc.org/lex v1.1.0
	modernc.org/lexer v1.0.2
	modernc.org/mathutil v1.4.1
	modernc.org/sortutil v1.1.0
	modernc.org/strutil v1.1.1
	modernc.org/token v1.0.0
	modernc.org/y v1.0.1
)
