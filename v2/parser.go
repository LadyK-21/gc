// Copyright 2022 The Gc Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package gc // import "modernc.org/gc/v2"

import (
	"fmt"
)

var (
	trcError bool // testing
)

type parser struct {
	cfg       *ParseSourceFileConfig
	loophacks []bool
	s         *Scanner

	loophack bool
}

func newParser(cfg *ParseSourceFileConfig, s *Scanner) *parser {
	s.Scan()
	return &parser{cfg: cfg, s: s}
}

func (p *parser) Err() error                          { return p.s.Err() }
func (p *parser) ch() Ch                              { return p.s.Tok.Ch }
func (p *parser) err(msg string, args ...interface{}) { p.errNode(p.s.Tok, msg, args...) }

func (p *parser) errNode(n Node, msg string, args ...interface{}) {
	if trcError {
		trc("ERROR %s", fmt.Sprintf(msg, args...))
	}
	p.s.errs.err(n.Position(), msg, args...)
	if !p.cfg.AllErrors && len(p.s.errs) >= 10 {
		p.s.close()
	}
}

func (p *parser) must(c Ch) (r Token) {
	if p.ch() != c {
		p.err(errorf("expected %v, got %v", c.str(), p.ch().str()))
	}
	return p.shift()
}

func (p *parser) opt(c Ch) (r Token) {
	if p.ch() == c {
		r = p.shift()
	}
	return r
}

func (p *parser) shift() (r Token) {
	r = p.s.Tok
	p.s.Scan()
	switch p.ch() {
	case FOR, IF, SELECT, SWITCH:
		p.loophack = true
	case '(', '[':
		if p.loophack || len(p.loophacks) != 0 {
			p.loophacks = append(p.loophacks, p.loophack)
			p.loophack = false
		}
	case ')', ']':
		if n := len(p.loophacks); n != 0 {
			p.loophack = p.loophacks[n-1]
			p.loophacks = p.loophacks[:n-1]
		}
	case '{':
		if p.loophack {
			p.s.Tok.Ch = body
			p.loophack = false
		}
	}
	// trc("SHIFT %v (%v: %v: %v)", r, origin(4), origin(3), origin(2))
	return r
}

func (p *parser) lbrace(lbr *bool) (r Token) {
	switch p.ch() {
	case '{':
		return p.shift()
	case body:
		r = p.shift()
		r.Ch = '{'
		if lbr != nil {
			*lbr = true
		}
		return r
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) fixlbr(lbr bool) (r Token) {
	if lbr {
		p.loophack = true
	}
	return p.must('}')
}

func (p *parser) semi(enabled bool) (r Token) {
	if enabled {
		switch p.ch() {
		case ';':
			r = p.shift()
		case ')', '}':
			// Specs: To allow complex statements to occupy a single line, a semicolon may
			// be omitted before a closing ")" or "}".
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
		}
	}
	return r
}

// TopLevelDecl = Declaration
// 	| FunctionDecl
// 	| MethodDecl .
func (p *parser) topLevelDecls() (r []Node) {
	//              TopLevelDecl case CONST, FUNC, TYPE, VAR:
	for {
		switch p.ch() {
		case CONST:
			r = append(r, p.constDecl())
		case FUNC:
			f := p.shift()
			switch p.ch() {
			case IDENTIFIER:
				r = append(r, p.functionDecl(f))
			case '(':
				r = append(r, p.methodDecl(f))
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		case TYPE:
			r = append(r, p.typeDecl())
		case VAR:
			r = append(r, p.varDecl())
		default:
			return r
		}
	}
}

// ConstDecl = "const" "(" ")"
// 	| "const" "(" ConstSpec ConstDecl_1 ";" ")"
// 	| "const" "(" ConstSpec ConstDecl_2 ")"
// 	| "const" ConstSpec .
// ConstDecl_1 =
// 	| ConstDecl_1 ";" ConstSpec .
// ConstDecl_2 =
// 	| ConstDecl_2 ";" ConstSpec .
func (p *parser) constDecl() (r *ConstDecl) {
	c := p.must(CONST)
	switch p.ch() {
	case '(':
		return &ConstDecl{Const: c, LParen: p.shift(), ConstSpecs: p.constSpecs(), RParen: p.must(')'), Semicolon: p.semi(true)}
	//                 ConstSpec
	case IDENTIFIER:
		return &ConstDecl{Const: c, ConstSpecs: []*ConstSpec{p.constSpec(false)}, Semicolon: p.semi(true)}
	default:
		_ = c
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) constSpecs() (r []*ConstSpec) {
	for p.ch() == IDENTIFIER {
		r = append(r, p.constSpec(true))
	}
	return r
}

// ConstSpec = IdentifierList "=" ExpressionList
// 	| IdentifierList Type "=" ExpressionList
// 	| IdentifierList .
func (p *parser) constSpec(semi bool) (r *ConstSpec) {
	if p.ch() != IDENTIFIER {
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}

	r = &ConstSpec{IdentifierList: p.identifierList()}
	switch p.ch() {
	case '=':
		r.Eq = p.shift()
		r.ExpressionList = p.expressionList()
	//                      Type
	case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		r.Type = p.type1()
		r.Eq = p.shift()
		r.ExpressionList = p.expressionList()
	case ';':
		// ok
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	r.Semicolon = p.semi(semi)
	return r
}

// TypeDecl = "type" TypeSpec
// 	| "type" "(" TypeDecl_1 ")" .
// TypeDecl_1 =
// 	| TypeDecl_1 TypeSpec ";" .
func (p *parser) typeDecl() (r *TypeDecl) {
	t := p.must(TYPE)
	switch p.ch() {
	case '(':
		return &TypeDecl{Type: t, LParen: p.shift(), TypeSpecs: p.typeSpecs(), RParen: p.must(')'), Semicolon: p.semi(true)}
	//                  TypeSpec
	case IDENTIFIER:
		return &TypeDecl{Type: t, TypeSpecs: []Node{p.typeSpec(false)}, Semicolon: p.semi(true)}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) typeSpecs() (r []Node) {
	//                  TypeSpec case IDENTIFIER:
	for p.ch() == IDENTIFIER {
		r = append(r, p.typeSpec(true))
	}
	return r
}

// TypeSpec = AliasDecl | TypeDef .
// AliasDecl = identifier "=" Type .
// TypeDef = identifier TypeDef_1 Type .
// TypeDef_1 =
// 	| TypeParameters .
// TypeParameters = "[" TypeParamList TypeParameters_1 "]" .
// TypeParameters_1 =
// 	| "," .
func (p *parser) typeSpec(semi bool) (r Node) {
	switch p.ch() {
	case IDENTIFIER:
		id := p.shift()
		// identifier .
		switch p.ch() {
		case '=':
			return &AliasDecl{Ident: id, Eq: p.shift(), Type: p.type1(), Semicolon: p.semi(semi)}
		case '[':
			lbracket := p.shift()
			// identifier "[" .
			switch p.ch() {
			case ']':
				// identifier "[" . "]"
				return &TypeDef{Ident: id, Type: &SliceType{LBracket: lbracket, RBracket: p.shift(), ElementType: p.type1()}, Semicolon: p.semi(semi)}
			case ELLIPSIS:
				// identifier "[" . "..."
				return &TypeDef{Ident: id, Type: &ArrayType{LBracket: lbracket, ArrayLength: p.shift(), RBracket: p.must(']'), ElementType: p.type1()}, Semicolon: p.semi(semi)}
			default:
				expr := p.expression(nil)
				// identifier "[" expression .
				switch p.ch() {
				case ']':
					// identifier "[" expression . "]"
					return &TypeDef{Ident: id, Type: &ArrayType{LBracket: lbracket, ArrayLength: expr, RBracket: p.shift(), ElementType: p.type1()}, Semicolon: p.semi(semi)}
				default:
					switch x := expr.(type) {
					case Token:
						return &TypeDef{Ident: id, TypeParameters: p.typeParameters2(lbracket, x), Type: p.type1(), Semicolon: p.semi(semi)}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				}
			}
			//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		case '(', '*', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			return &TypeDef{Ident: id, Type: p.type1(), Semicolon: p.semi(semi)}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// TypeParameters = "[" TypeParamList TypeParameters_1 "]" .
// TypeParameters_1 =
// 	| "," .
func (p *parser) typeParameters() (r *TypeParameters) {
	return &TypeParameters{LBracket: p.must('['), TypeParamList: p.typeParamDecls(), RBracket: p.must(']')}
}

// TypeParamDecl = IdentifierList TypeConstraint .
func (p *parser) typeParamDecls() (r []*TypeParamDecl) {
	for {
		switch p.ch() {
		//            IdentifierList
		case IDENTIFIER:
			r = append(r, &TypeParamDecl{IdentifierList: p.identifierList(), TypeConstraint: p.typeElem(false), Comma: p.opt(',')})
		default:
			return r
		}
	}
}

// identifier "[" identifier .
func (p *parser) typeParameters2(lbracket, id Token) (r *TypeParameters) {
	return &TypeParameters{LBracket: lbracket, TypeParamList: p.typeParamDecls2(p.identifierList2(id)), RBracket: p.must(']')}
}

func (p *parser) typeParamDecls2(il []*IdentifierListItem) (r []*TypeParamDecl) {
	r = []*TypeParamDecl{{IdentifierList: il, TypeConstraint: p.typeElem(false), Comma: p.opt(',')}}
	for {
		switch p.ch() {
		//            IdentifierList
		case IDENTIFIER:
			r = append(r, &TypeParamDecl{IdentifierList: p.identifierList(), TypeConstraint: p.typeElem(false), Comma: p.opt(',')})
		default:
			return r
		}
	}
}

// MethodDecl = "func" Receiver MethodName Signature MethodDecl_1 .
// MethodDecl_1 =
// 	| FunctionBody .
func (p *parser) methodDecl(f Token) (r *MethodDecl) {
	r = &MethodDecl{Func: f, Receiver: p.parameters(), MethodName: p.must(IDENTIFIER)}
	switch p.ch() {
	//                 Signature
	case '(':
		r.Signature = p.signature()
	//            TypeParameters case '[':
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	if p.ch() == '{' {
		r.FunctionBody = p.block(false)
	}
	r.Semicolon = p.semi(true)
	return r
}

// FunctionDecl = "func" FunctionName TypeParameters Signature FunctionDecl_1
// 	| "func" FunctionName Signature FunctionDecl_2 .
// FunctionDecl_1 =
// 	| FunctionBody .
// FunctionDecl_2 =
// 	| FunctionBody .
func (p *parser) functionDecl(f Token) (r *FunctionDecl) {
	r = &FunctionDecl{Func: f, FunctionName: p.must(IDENTIFIER)}
	switch p.ch() {
	//                 Signature
	case '(':
		r.Signature = p.signature()
		//            TypeParameters
	case '[':
		r.TypeParameters = p.typeParameters()
		r.Signature = p.signature()
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	if p.ch() == '{' {
		r.FunctionBody = p.block(false)
	}
	r.Semicolon = p.semi(true)
	return r
}

// Block = "{" StatementList "}" .
func (p *parser) block(semi bool) (r *Block) {
	return &Block{LBrace: p.must('{'), StatementList: p.statementList(), RBrace: p.must('}'), Semicolon: p.semi(semi)}
}

// Signature = Parameters Signature_1 .
// Signature_1 =
// 	| Result .
func (p *parser) signature() (r *Signature) {
	r = &Signature{Parameters: p.parameters()}
	switch p.ch() {
	//                    Result
	case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		r.Result = p.result()
		return r
	default:
		return r
	}
}

// Result = Parameters | Type .
func (p *parser) result() (r Node) {
	switch p.ch() {
	//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
	//                Parameters case '(':
	case '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		return p.type1()
	case '(':
		return p.parameters()
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// Parameters = "(" Parameters_1 ")" .
// Parameters_1 =
// 	| ParameterList Parameters_1_1 .
// Parameters_1_1 =
// 	| "," .
func (p *parser) parameters() (r *Parameters) {
	return &Parameters{LParen: p.must('('), ParameterList: p.parameterList(), Comma: p.opt(','), RParen: p.must(')')}
}

// ParameterDecl = identifier "..." Type
// 	| identifier Type
// 	| "..." Type
// 	| Type .
func (p *parser) parameterList() (r []*ParameterDecl) {
	for {
		switch p.ch() {
		case IDENTIFIER:
			id := p.shift()
			// identifier .
			switch p.ch() {
			case '(', '*', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
				r = append(r, &ParameterDecl{IdentifierList: []*IdentifierListItem{{Ident: id}}, Type: p.type1(), Comma: p.opt(',')})
			case ELLIPSIS:
				r = append(r, &ParameterDecl{IdentifierList: []*IdentifierListItem{{Ident: id}}, Ellipsis: p.shift(), Type: p.type1(), Comma: p.opt(',')})
			case ',':
				r = append(r, &ParameterDecl{Type: id, Comma: p.shift()})
			case ')':
				return append(r, &ParameterDecl{Type: id})
			case '.':
				r = append(r, &ParameterDecl{Type: p.typeName2(&TypeName{Name: &QualifiedIdent{PackageName: id, Dot: p.shift(), Ident: p.must(IDENTIFIER)}}), Comma: p.opt(',')})
			case '[':
				lbracket := p.shift()
				// identifier "[" .
				switch p.ch() {
				case ']':
					// identifier "[" . "]"
					r = append(r, &ParameterDecl{IdentifierList: []*IdentifierListItem{{Ident: id}}, Type: &SliceType{LBracket: lbracket, RBracket: p.must(']'), ElementType: p.type1()}, Comma: p.opt(',')})

				default:
					switch x := p.exprOrType().(type) {
					case typ:
						p.err(errorf("TODO %v", p.ch().str()))
						p.shift()
						return r
					default:
						// identifier "[" expression .
						switch p.ch() {
						case ']':
							rbracket := p.shift()
							// identifier "[" expression "]" .
							switch p.ch() {
							case ')':
								// identifier "[" expression "]" . ")"
								return append(r, &ParameterDecl{Type: &TypeName{Name: &QualifiedIdent{Ident: id}, TypeArgs: &TypeArgs{LBracket: lbracket, TypeList: []*TypeListItem{{Type: x}}, RBracket: rbracket}}, Comma: p.opt(',')})
							//                      Type
							case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
								// identifier "[" expression "]" . Type
								r = append(r, &ParameterDecl{IdentifierList: []*IdentifierListItem{{Ident: id}}, Type: &ArrayType{LBracket: lbracket, ArrayLength: x, RBracket: rbracket, ElementType: p.type1()}, Comma: p.opt(',')})
							default:
								p.err(errorf("TODO %v", p.ch().str()))
								p.shift()
								return r
							}
						case ',':
							// identifier "[" expression . ","
							r = append(r, &ParameterDecl{Type: p.typeName2(&TypeName{Name: &QualifiedIdent{Ident: id}, TypeArgs: p.typeArgs2(lbracket, x)})})
						default:
							p.err(errorf("TODO %v", p.ch().str()))
							p.shift()
							return r
						}
					}
				}
			default:
				p.err(errorf("TODO %v", p.ch().str()))
				p.shift()
				return r
			}
		case ELLIPSIS:
			r = append(r, &ParameterDecl{Ellipsis: p.shift(), Type: p.type1(), Comma: p.opt(',')})
		//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		case '(', '*', '[', ARROW, CHAN, FUNC, INTERFACE, MAP, STRUCT:
			r = append(r, &ParameterDecl{Type: p.type1(), Comma: p.opt(',')})
		default:
			return r
		}
	}
}

// VarDecl = "var" VarSpec
// 	| "var" "(" VarDecl_1 ")" .
// VarDecl_1 =
// 	| VarDecl_1 VarSpec ";" .
func (p *parser) varDecl() (r *VarDecl) {
	//                   VarDecl case VAR:
	v := p.must(VAR)
	switch p.ch() {
	case '(':
		return &VarDecl{Var: v, LParen: p.shift(), VarSpecs: p.varSpecs(), RParen: p.must(')'), Semicolon: p.semi(true)}
	//                   VarSpec
	case IDENTIFIER:
		return &VarDecl{Var: v, VarSpecs: []*VarSpec{p.varSpec(false)}, Semicolon: p.semi(true)}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) varSpecs() (r []*VarSpec) {
	for {
		switch p.ch() {
		//                   VarSpec
		case IDENTIFIER:
			r = append(r, p.varSpec(true))
		default:
			return r
		}
	}
}

// VarSpec = IdentifierList Type VarSpec_1
// 	| IdentifierList "=" ExpressionList .
// VarSpec_1 =
// 	| "=" ExpressionList .
func (p *parser) varSpec(semi bool) (r *VarSpec) {
	switch p.ch() {
	//                   VarSpec
	case IDENTIFIER:
		r = &VarSpec{IdentifierList: p.identifierList()}
		switch p.ch() {
		case '=':
			r.Eq = p.shift()
			r.ExpressionList = p.expressionList()
		//                      Type
		case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			r.Type = p.type1()
			switch p.ch() {
			case '=':
				r.Eq = p.shift()
				r.ExpressionList = p.expressionList()
			}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	r.Semicolon = p.semi(semi)
	return r
}

// IdentifierList = identifier IdentifierList_1 .
// IdentifierList_1 =
// 	| IdentifierList_1 "," identifier .
func (p *parser) identifierList() (r []*IdentifierListItem) {
	for p.ch() == IDENTIFIER {
		n := &IdentifierListItem{Ident: p.shift()}
		r = append(r, n)
		switch p.ch() {
		case ',':
			n.Comma = p.shift()
		default:
			return r
		}
	}
	return r
}

// identifier .
func (p *parser) identifierList2(id Token) (r []*IdentifierListItem) {
	n := &IdentifierListItem{Ident: id}
	r = append(r, n)
	if p.ch() != ',' {
		return r
	}

	n.Comma = p.shift()
	return append(r, p.identifierList()...)
}

// ImportDecl = "import" ImportSpec
// 	| "import" "(" ImportDecl_1 ")" .
// ImportDecl_1 =
// 	| ImportDecl_1 ImportSpec ";" .
func (p *parser) importDecls() (r []*ImportDecl) {
	for {
		switch p.ch() {
		//                ImportDecl
		case IMPORT:
			im := p.shift()
			switch p.ch() {
			case '(':
				r = append(r, &ImportDecl{Import: im, LParen: p.shift(), ImportSpecs: p.importSpecs(), RParen: p.must(')'), Semicolon: p.semi(true)})
			//                ImportSpec
			case '.', IDENTIFIER, STRING_LIT:
				r = append(r, &ImportDecl{Import: im, ImportSpecs: []*ImportSpec{p.importSpec(false)}, Semicolon: p.semi(true)})
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			return r
		}
	}
}

func (p *parser) importSpecs() (r []*ImportSpec) {
	for {
		switch p.ch() {
		//                ImportSpec
		case '.', IDENTIFIER, STRING_LIT:
			r = append(r, p.importSpec(true))
		default:
			return r
		}
	}
}

// ImportSpec = "." ImportPath
// 	| PackageName ImportPath
// 	| ImportPath .
func (p *parser) importSpec(semi bool) (r *ImportSpec) {
	//                ImportSpec case '.', IDENTIFIER, STRING_LIT:
	switch p.ch() {
	case '.', IDENTIFIER:
		return &ImportSpec{Qualifier: p.shift(), ImportPath: p.shift(), Semicolon: p.semi(semi)}
	case STRING_LIT:
		return &ImportSpec{ImportPath: p.shift(), Semicolon: p.semi(semi)}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// ------------------------------------------------------------------ Statemens

// StatementList = StatementList_1 StatementList_2 .
// StatementList_1 =
// 	| StatementList_1 StatementList_1_1 ";" .
// StatementList_1_1 =
// 	| Statement .
// StatementList_2 =
// 	| Statement .
func (p *parser) statementList() (r []Node) {
	for {
		switch p.ch() {
		//                 Statement
		case '!', '&', '(', '*', '+', '-', '[', '^', '{', ARROW, BREAK, CHAN, CONST, CONTINUE, DEFER, FALLTHROUGH, FLOAT_LIT, FOR, FUNC, GO, GOTO, IDENTIFIER, IF, IMAG_LIT, INTERFACE, INT_LIT, MAP, RETURN, RUNE_LIT, SELECT, STRING_LIT, STRUCT, SWITCH, TYPE, VAR:
			r = append(r, p.statement())
		case ';':
			r = append(r, &EmptyStmt{Semicolon: p.shift()})
		default:
			return r
		}
	}
}

func (p *parser) statement() (r Node) {
	switch p.ch() {
	case IDENTIFIER:
		switch x := p.exprOrSimpleStmt(false).(type) {
		case simpleStmt:
			x.semi(p)
			return x
		case nil:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		default:
			switch p.ch() {
			case ':':
				id, ok := x.(Token)
				if !ok {
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}

				colon := p.shift()
				switch p.ch() {
				case '}':
					return &LabeledStmt{Label: id, Colon: colon, Statement: &EmptyStmt{}}
				default:
					return &LabeledStmt{Label: id, Colon: colon, Statement: p.statement()}
				}
			default:
				return &ExpressionStmt{Expression: x, Semicolon: p.semi(true)}
			}
		}
	}
	return p.statement2()
}

func (p *parser) statement2() (r Node) {
	switch p.ch() {
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		return p.simpleStmt(true)
	case '{':
		return p.block(true)
	case BREAK:
		return &BreakStmt{Break: p.shift(), Label: p.opt(IDENTIFIER), Semicolon: p.semi(true)}
	case CONST:
		return p.constDecl()
	case CONTINUE:
		return &ContinueStmt{Continue: p.shift(), Label: p.opt(IDENTIFIER), Semicolon: p.semi(true)}
	case DEFER:
		return &DeferStmt{Defer: p.shift(), Expression: p.expression(nil), Semicolon: p.semi(true)}
	case FALLTHROUGH:
		return &FallthroughStmt{Fallthrough: p.shift(), Semicolon: p.semi(true)}
	case FOR:
		return p.forStmt()
	case GO:
		return &GoStmt{Go: p.shift(), Expression: p.expression(nil), Semicolon: p.semi(true)}
	case GOTO:
		return &GotoStmt{Goto: p.shift(), Label: p.must(IDENTIFIER), Semicolon: p.semi(true)}
	case IF:
		return p.ifStmt(true)
	case RETURN:
		n := &ReturnStmt{Return: p.shift()}
		switch p.ch() {
		//                Expression
		case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			n.ExpressionList = p.expressionList()
		}
		n.Semicolon = p.semi(true)
		return n
	case SELECT:
		return &SelectStmt{Select: p.shift(), LBrace: p.must(body), CommClauses: p.commClauses(), RBrace: p.must('}'), Semicolon: p.semi(true)}
	case SWITCH:
		return p.switchStmt()
	case TYPE:
		return p.typeDecl()
	case VAR:
		return p.varDecl()
	case ';':
		return &EmptyStmt{Semicolon: p.shift()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// CommClause = CommCase ":" StatementList .
func (p *parser) commClauses() (r []*CommClause) {
	for {
		switch p.ch() {
		//                  CommCase
		case CASE, DEFAULT:
			r = append(r, &CommClause{CommCase: p.commCase(), Colon: p.must(':'), StatementList: p.statementList()})
		default:
			return r
		}
	}
}

// CommCase = "case" SendStmt
// 	| "case" RecvStmt
// 	| "default" .
// RecvStmt   = [ ExpressionList "=" | IdentifierList ":=" ] RecvExpr .
// SendStmt = Channel "<-" Expression .
func (p *parser) commCase() (r *CommCase) {
	switch p.ch() {
	case CASE:
		case1 := p.shift()
		switch p.ch() {
		//                SimpleStmt
		case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			return &CommCase{CaseOrDefault: case1, Statement: p.simpleStmt(false)}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case DEFAULT:
		return &CommCase{CaseOrDefault: p.shift()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) simpleStmt(semi bool) (r Node) {
	switch x := p.exprOrSimpleStmt(semi).(type) {
	case simpleStmt:
		return x
	case nil:
		return nil
	default:
		return &ExpressionStmt{Expression: x, Semicolon: p.semi(semi)}
	}
}

// ForStmt = "for" ForClause LoopBody
// 	| "for" RangeClause LoopBody
// 	| "for" Condition LoopBody
// 	| "for" LoopBody .
func (p *parser) forStmt() (r *ForStmt) {
	//        Condition case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	//        ForClause case '!', '&', '(', '*', '+', '-', ';', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	//      RangeClause case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RANGE, RUNE_LIT, STRING_LIT, STRUCT:
	r = &ForStmt{For: p.must(FOR)}
	switch p.ch() {
	// case ';':
	// 	p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
	// 	p.shift()
	// 	return r
	// case RANGE:
	// 	p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
	// 	p.shift()
	// 	return r
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		expr := p.expression(nil)
		switch p.ch() {
		case ',':
			comma := p.shift()
			el := append([]*ExpressionListItem{{Expression: expr, Comma: comma}}, p.expressionList()...)
			switch p.ch() {
			case DEFINE:
				def := p.shift()
				switch p.ch() {
				case RANGE:
					r.RangeClause = &RangeClause{ExpressionList: el, Assign: def, Range: p.shift(), Expression: p.expression(nil)}
				//                Expression
				case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
					r.ForClause = p.forClause(&ShortVarDecl{IdentifierList: p.exprListToIdList(el), Define: def, ExpressionList: p.expressionList()})
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			case '=', ADD_ASSIGN, AND_ASSIGN, AND_NOT_ASSIGN, MUL_ASSIGN, OR_ASSIGN, QUO_ASSIGN, REM_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, SUB_ASSIGN, XOR_ASSIGN:
				op := p.shift()
				switch p.ch() {
				case RANGE:
					r.RangeClause = &RangeClause{ExpressionList: el, Assign: op, Range: p.shift(), Expression: p.expression(nil)}
				//                Expression
				case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
					r.ForClause = p.forClause(&Assignment{LExpressionList: el, AssOp: op, RExpressionList: p.expressionList()})
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		case DEFINE:
			def := p.shift()
			switch p.ch() {
			case RANGE:
				r.RangeClause = &RangeClause{ExpressionList: []*ExpressionListItem{{Expression: expr}}, Assign: def, Range: p.shift(), Expression: p.expression(nil)}
			default:
				expr2 := p.expression(nil)
				switch p.ch() {
				case ';':
					r.ForClause = p.forClause(&ShortVarDecl{IdentifierList: p.exprToIdList(expr), Define: def, ExpressionList: []*ExpressionListItem{{Expression: expr2}}})
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			}
		case '=', ADD_ASSIGN, AND_ASSIGN, AND_NOT_ASSIGN, MUL_ASSIGN, OR_ASSIGN, QUO_ASSIGN, REM_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, SUB_ASSIGN, XOR_ASSIGN:
			op := p.shift()
			switch p.ch() {
			case RANGE:
				r.RangeClause = &RangeClause{ExpressionList: []*ExpressionListItem{{Expression: expr}}, Assign: op, Range: p.shift(), Expression: p.expression(nil)}
			default:
				expr2 := p.expression(nil)
				switch p.ch() {
				case ';':
					r.ForClause = p.forClause(&Assignment{LExpressionList: []*ExpressionListItem{{Expression: expr}}, AssOp: op, RExpressionList: []*ExpressionListItem{{Expression: expr2}}})
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			}
		case body, '{':
			r.ForClause = &ForClause{Condition: expr}
		case INC, DEC:
			r.ForClause = p.forClause(&IncDecStmt{Expression: expr, Op: p.shift()})
		case ';':
			r.ForClause = p.forClause(expr)
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case '{', body:
		// ok
	case ';':
		r.ForClause = p.forClause(nil)
	case RANGE:
		r.RangeClause = &RangeClause{Range: p.shift(), Expression: p.expression(nil)}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	r.Block = p.loopBody()
	r.Semicolon = p.semi(true)
	return r
}

func (p *parser) forClause(init Node) (r *ForClause) {
	r = &ForClause{InitStmt: init, Semicolon: p.must(';')}
	switch p.ch() {
	case ';':
		// ok
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		r.Condition = p.expression(nil)
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	r.Semicolon2 = p.must(';')
	switch p.ch() {
	//                SimpleStmt
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		r.PostStmt = p.simpleStmt(false)
	}
	return r
}

// SwitchStmt = ExprSwitchStmt | TypeSwitchStmt .
// ExprSwitchStmt = "switch" ExprSwitchStmt_1 ExprSwitchStmt_2 body ExprSwitchStmt_3 "}" .
// ExprSwitchStmt_1 =
// 	| SimpleStmt ";" .
// ExprSwitchStmt_2 =
// 	| Expression .
// ExprSwitchStmt_3 =
// 	| ExprSwitchStmt_3 ExprCaseClause .
// TypeSwitchStmt = "switch" TypeSwitchStmt_1 TypeSwitchGuard body TypeSwitchStmt_2 "}" .
// TypeSwitchStmt_1 =
// 	| SimpleStmt ";" .
// TypeSwitchStmt_2 =
// 	| TypeSwitchStmt_2 TypeCaseClause .
func (p *parser) switchStmt() (r Node) {
	sw := p.must(SWITCH)
	// "switch" .
	switch p.ch() {
	//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	//                SimpleStmt case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		switch x := p.exprOrSimpleStmt(false).(type) {
		case simpleStmt:
			switch p.ch() {
			case body:
				switch y := x.(type) {
				case *ShortVarDecl:
					if len(y.ExpressionList) != 1 {
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}

					switch z := y.ExpressionList[0].Expression.(type) {
					case *TypeSwitchGuard:
						if len(y.IdentifierList) != 1 {
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}

						// "switch" 'foo := bar.(type)' body
						z.Ident = y.IdentifierList[0].Ident
						z.Define = y.Define
						return &TypeSwitchStmt{Switch: sw, TypeSwitchGuard: z, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			case ';':
				semi := p.shift()
				// "switch" foo := bar ";" .
				switch p.ch() {
				case body:
					return &ExpressionSwitchStmt{Switch: sw, SimpleStmt: x, Semicolon: semi, LBrace: p.body(), ExprCaseClauses: p.exprCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
					//                Expression
				case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
					expr := p.expression(nil)
					if y, ok := expr.(*TypeSwitchGuard); ok {
						// "switch" foo := bar ";" Expression.(type) .
						return &TypeSwitchStmt{Switch: sw, SimpleStmt: x, Semicolon: semi, TypeSwitchGuard: y, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
					}

					switch p.ch() {
					case DEFINE:
						t, ok := expr.(Token)
						if !ok {
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}

						def := p.shift()
						switch y := p.primaryExpression().(type) {
						case *TypeSwitchGuard:
							y.Ident = t
							y.Define = def
							return &TypeSwitchStmt{Switch: sw, SimpleStmt: x, Semicolon: semi, TypeSwitchGuard: y, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
						default:
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}
					case body:
						return &ExpressionSwitchStmt{Switch: sw, SimpleStmt: x, Semicolon: semi, Expression: expr, LBrace: p.body(), ExprCaseClauses: p.exprCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			case '.':
				switch y := x.(type) {
				case *ShortVarDecl:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
					if len(y.IdentifierList) != 1 || len(y.ExpressionList) != 1 {
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}

					g := &TypeSwitchGuard{Ident: y.IdentifierList[0].Ident, Define: y.Define, PrimaryExpr: y.ExpressionList[0], Dot: p.shift(), LParen: p.must('('), Type: p.must(TYPE), RParen: p.must(')')}
					return &TypeSwitchStmt{Switch: sw, TypeSwitchGuard: g, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
				default:
					p.err(errorf("TODO %T", y))
					p.shift()
					return r
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			switch y := x.(type) {
			case *TypeSwitchGuard:
				return &TypeSwitchStmt{Switch: sw, TypeSwitchGuard: y, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
			default:
				switch p.ch() {
				case body:
					return &ExpressionSwitchStmt{Switch: sw, Expression: x, LBrace: p.body(), ExprCaseClauses: p.exprCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
				case ';':
					semi := p.shift()
					switch p.ch() {
					case body:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					default:
						_ = semi
						switch z := p.expression(nil).(type) {
						case *TypeSwitchGuard:
							return &TypeSwitchStmt{Switch: sw, SimpleStmt: y, Semicolon: semi, TypeSwitchGuard: z, LBrace: p.body(), TypeCaseClauses: p.typeCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
						default:
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}
					}
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			}
		}
	case body:
		// ExprSwitchStmt = "switch" ExprSwitchStmt_1 ExprSwitchStmt_2 body ExprSwitchStmt_3 "}" .
		// ExprSwitchStmt_1 =
		// 	| SimpleStmt ";" .
		// ExprSwitchStmt_2 =
		// 	| Expression .
		// ExprSwitchStmt_3 =
		// 	| ExprSwitchStmt_3 ExprCaseClause .
		return &ExpressionSwitchStmt{Switch: sw, LBrace: p.body(), ExprCaseClauses: p.exprCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
	case ';':
		semi := p.shift()
		switch p.ch() {
		case body:
			return &ExpressionSwitchStmt{Switch: sw, SimpleStmt: &EmptyStmt{}, Semicolon: semi, LBrace: p.body(), ExprCaseClauses: p.exprCaseClauses(), RBrace: p.must('}'), Semicolon2: p.semi(true)}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// TypeCaseClause = TypeSwitchCase ":" StatementList .
func (p *parser) typeCaseClauses() (r []*TypeCaseClause) {
	for {
		switch p.ch() {
		//            TypeSwitchCase
		case CASE, DEFAULT:
			r = append(r, &TypeCaseClause{TypeSwitchCase: p.typeSwitchCase(), Colon: p.must(':'), StatementList: p.statementList()})
		default:
			return r
		}
	}
}

// TypeSwitchCase = "case" TypeList
// 	| "default" .
func (p *parser) typeSwitchCase() (r *TypeSwitchCase) {
	switch p.ch() {
	case CASE:
		return &TypeSwitchCase{CaseOrDefault: p.shift(), TypeList: p.typeList()}
	case DEFAULT:
		return &TypeSwitchCase{CaseOrDefault: p.shift()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) typeList() (r []*TypeListItem) {
	for {
		switch p.ch() {
		//                      Type
		case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			r = append(r, &TypeListItem{Type: p.type1(), Comma: p.opt(',')})
		default:
			return r
		}
	}
}

func (p *parser) typeList2(typ Node) (r []*TypeListItem) {
	if p.ch() != ',' {
		return []*TypeListItem{{Type: typ}}
	}

	r = []*TypeListItem{{Type: typ, Comma: p.shift()}}
	for {
		switch p.ch() {
		//                      Type
		case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			r = append(r, &TypeListItem{Type: p.type1(), Comma: p.opt(',')})
		default:
			return r
		}
	}
}

// ExprCaseClause = ExprSwitchCase ":" StatementList .
func (p *parser) exprCaseClauses() (r []*ExprCaseClause) {
	for {
		switch p.ch() {
		//            ExprSwitchCase
		case CASE, DEFAULT:
			r = append(r, &ExprCaseClause{ExprSwitchCase: p.exprSwitchCase(), Colon: p.must(':'), StatementList: p.statementList()})
		default:
			return r
		}
	}
}

// ExprSwitchCase = "case" ExpressionList
// 	| "default" .
func (p *parser) exprSwitchCase() (r *ExprSwitchCase) {
	switch p.ch() {
	case CASE:
		return &ExprSwitchCase{CaseOrDefault: p.shift(), ExpressionList: p.expressionList()}
	case DEFAULT:
		return &ExprSwitchCase{CaseOrDefault: p.shift()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// IfStmt = "if" IfStmt_1 Expression LoopBody IfStmt_2 .
// IfStmt_1 =
// 	| SimpleStmt ";" .
// IfStmt_2 =
// 	| "else" IfStmt_2_1 .
// IfStmt_2_1 = IfStmt | Block .
func (p *parser) ifStmt(semi bool) (r *IfStmt) {
	//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	//                SimpleStmt case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	r = &IfStmt{If: p.must(IF)}
	switch x := p.exprOrSimpleStmt(false).(type) {
	case simpleStmt:
		r.SimpleStmt = x
		r.Semicolon = p.must(';')
		r.Expression = p.expression(nil)
	default:
		switch p.ch() {
		case ';':
			r.SimpleStmt = x
			r.Semicolon = p.shift()
			r.Expression = p.expression(nil)
		case body:
			r.Expression = x
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	}
	r.Block = p.loopBody()
	switch p.ch() {
	case ELSE:
		r.Else = p.shift()
		switch p.ch() {
		case '{':
			r.ElsePart = p.block(false)
		case IF:
			r.ElsePart = p.ifStmt(false)
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	}
	r.Semicolon2 = p.semi(semi)
	return r
}

// LoopBody = body StatementList "}" .
func (p *parser) loopBody() (r *Block) {
	return &Block{LBrace: p.body(), StatementList: p.statementList(), RBrace: p.must('}')}
}

func (p *parser) body() (r Token) {
	switch p.ch() {
	case body:
		r = p.shift()
		r.Ch = '{'
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
	}
	return r
}

func (p *parser) exprOrSimpleStmt(semi bool) (r Node) {
	//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	//                SimpleStmt case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	switch p.ch() {
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		expr := p.expression(nil)
		switch p.ch() {
		case body:
			return expr
		case DEFINE:
			return &ShortVarDecl{IdentifierList: p.exprListToIdList([]*ExpressionListItem{{Expression: expr}}), Define: p.shift(), ExpressionList: p.expressionList(), Semicolon: p.semi(semi)}
		//                 assign_op
		case '=', ADD_ASSIGN, AND_ASSIGN, AND_NOT_ASSIGN, MUL_ASSIGN, OR_ASSIGN, QUO_ASSIGN, REM_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, SUB_ASSIGN, XOR_ASSIGN:
			return &Assignment{LExpressionList: []*ExpressionListItem{{Expression: expr}}, AssOp: p.shift(), RExpressionList: p.expressionList(), Semicolon: p.semi(semi)}
		case ',':
			comma := p.shift()
			el := append([]*ExpressionListItem{{Expression: expr, Comma: comma}}, p.expressionList()...)
			switch p.ch() {
			case DEFINE:
				return &ShortVarDecl{IdentifierList: p.exprListToIdList(el), Define: p.shift(), ExpressionList: p.expressionList(), Semicolon: p.semi(semi)}
			//                 assign_op
			case '=', ADD_ASSIGN, AND_ASSIGN, AND_NOT_ASSIGN, MUL_ASSIGN, OR_ASSIGN, QUO_ASSIGN, REM_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, SUB_ASSIGN, XOR_ASSIGN:
				return &Assignment{LExpressionList: el, AssOp: p.shift(), RExpressionList: p.expressionList(), Semicolon: p.semi(semi)}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		case ';', '}', ']', ':':
			return expr
		case INC, DEC:
			return &IncDecStmt{Expression: expr, Op: p.shift(), Semicolon: p.semi(semi)}
		case ARROW:
			return &SendStmt{Channel: expr, Arrow: p.shift(), Expression: p.expression(nil), Semicolon: p.semi(semi)}

		case '.':
			// Expression "." "(" TYPE
			return expr
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case ';':
		return &EmptyStmt{Semicolon: p.semi(semi)}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

func (p *parser) exprToIdList(e Node) (r []*IdentifierListItem) {
	switch x := e.(type) {
	case Token:
		switch x.Ch {
		case IDENTIFIER:
			return []*IdentifierListItem{{Ident: x}}
		default:
			p.err(errorf("TODO %v", x))
			p.shift()
			return r
		}
	default:
		p.err(errorf("TODO %T", x))
		p.shift()
		return r
	}
}

func (p *parser) exprListToIdList(l []*ExpressionListItem) (r []*IdentifierListItem) {
	for _, v := range l {
		switch x := v.Expression.(type) {
		case Token:
			switch x.Ch {
			case IDENTIFIER:
				r = append(r, &IdentifierListItem{Ident: x, Comma: v.Comma})
			default:
				p.err(errorf("TODO %v", x))
				p.shift()
				return r
			}
		default:
			p.err(errorf("TODO %T", x))
			p.shift()
			return r
		}
	}
	return r
}

// ---------------------------------------------------------------------- Types

func (p *parser) type1() (r Node) {
	//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
	switch p.ch() {
	case '(':
		return &ParenType{LParen: p.shift(), Type: p.type1(), RParen: p.must(')')}
	case '*':
		return &PointerType{Star: p.shift(), BaseType: p.type1()}
	case '[':
		lbracket := p.shift()
		switch p.ch() {
		case ']':
			return &SliceType{LBracket: lbracket, RBracket: p.shift(), ElementType: p.type1()}
		//                Expression
		case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			return &ArrayType{LBracket: lbracket, ArrayLength: p.expression(nil), RBracket: p.must(']'), ElementType: p.type1()}
		case ELLIPSIS:
			return &ArrayType{LBracket: lbracket, ArrayLength: p.shift(), RBracket: p.must(']'), ElementType: p.type1()}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case ARROW, CHAN:
		return p.channelType()
	case FUNC:
		// FunctionType = "func" Signature .
		return &FunctionType{Func: p.shift(), Signature: p.signature()}
	case IDENTIFIER:
		return p.typeName()
	case INTERFACE:
		// InterfaceType = "interface" lbrace "#fixlbr" "}"
		// 	| "interface" lbrace InterfaceElem InterfaceType_1 InterfaceType_2 "#fixlbr" "}" .
		var lbr bool
		return &InterfaceType{Interface: p.shift(), LBrace: p.lbrace(&lbr), InterfaceElems: p.interfaceElems(), RBrace: p.fixlbr(lbr)}
	case MAP:
		return &MapType{Map: p.shift(), LBracket: p.must('['), KeyType: p.type1(), RBracket: p.must(']'), ElementType: p.type1()}
	case STRUCT:
		// StructType = "struct" lbrace "#fixlbr" "}"
		// 	| "struct" lbrace FieldDecl StructType_1 StructType_2 "#fixlbr" "}" .
		// StructType_1 =
		// 	| StructType_1 ";" FieldDecl .
		// StructType_2 =
		// 	| ";" .
		var lbr bool
		n := &StructType{Struct: p.shift(), LBrace: p.lbrace(&lbr)}
		if p.ch() == '}' {
			n.RBrace = p.fixlbr(lbr)
			return n
		}

		for {
			switch p.ch() {
			//             EmbeddedField case '*', IDENTIFIER:
			//            IdentifierList case IDENTIFIER:
			case '*', IDENTIFIER:
				n.FieldDecls = append(n.FieldDecls, p.fieldDecl())
			default:
				n.RBrace = p.fixlbr(lbr)
				return n
			}
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// ChannelType = "<-" "chan" ElementType
// 	| "chan" "<-" ElementType
// 	| "chan" ElementType .
func (p *parser) channelType() (r *ChannelType) {
	switch p.ch() {
	case ARROW:
		return &ChannelType{ArrowPre: p.shift(), Chan: p.shift(), ElementType: p.type1()}
	case CHAN:
		return &ChannelType{Chan: p.shift(), ArrayPost: p.opt(ARROW), ElementType: p.type1()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// FieldDecl = IdentifierList Type FieldDecl_1
// 	| EmbeddedField FieldDecl_2 .
// FieldDecl_1 =
// 	| Tag .
// FieldDecl_2 =
// 	| Tag .
func (p *parser) fieldDecl() (r *FieldDecl) {
	switch p.ch() {
	//             EmbeddedField case '*', IDENTIFIER:
	//            IdentifierList case IDENTIFIER:
	case IDENTIFIER:
		id := p.shift()
		//  identifier .
		switch p.ch() {
		case ',':
			//  identifier . ","
			r = &FieldDecl{IdentifierList: p.identifierList2(id), Type: p.type1()}
		case ';', '}':
			//  identifier . ";"
			r = &FieldDecl{EmbeddedField: &EmbeddedField{TypeName: &TypeName{Name: &QualifiedIdent{Ident: id}}}}
		case '.':
			//  identifier . "."
			r = &FieldDecl{EmbeddedField: &EmbeddedField{TypeName: p.typeName2(&TypeName{Name: &QualifiedIdent{PackageName: id, Dot: p.shift(), Ident: p.must(IDENTIFIER)}})}}
		case '[':
			lbracket := p.shift()
			//  identifier "[" .
			switch p.ch() {
			case ']':
				//  identifier "[" . "]"
				r = &FieldDecl{IdentifierList: p.identifierList2(id), Type: &SliceType{LBracket: lbracket, RBracket: p.must(']'), ElementType: p.type1()}}
				// case ']', FLOAT_LIT, IMAG_LIT, INT_LIT, RUNE_LIT, STRING_LIT:
				// 	// . identifier "[" "]"
				// 	r = &FieldDecl{IdentifierList: p.identifierList(), Type: p.type1()}
			case IDENTIFIER:
				// . identifier "[" . identifier
				switch x := p.exprOrType().(type) {
				case typ:
					// . identifier "[" Type .
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				default:
					// . identifier "[" Expression .
					switch p.ch() {
					case ']':
						rbracket := p.shift()
						// . identifier "[" Expression "]" .
						switch p.ch() {
						case ';', '}':
							// . identifier "[" Expression "]" . ";"
							r = &FieldDecl{EmbeddedField: &EmbeddedField{TypeName: &TypeName{Name: &QualifiedIdent{Ident: id}, TypeArgs: &TypeArgs{LBracket: lbracket, TypeList: []*TypeListItem{{Type: x}}, RBracket: rbracket}}}}
						default:
							r = &FieldDecl{IdentifierList: []*IdentifierListItem{{Ident: id}}, Type: &ArrayType{LBracket: lbracket, ArrayLength: x, RBracket: rbracket, ElementType: p.type1()}}
						}
					case ',':
						// . identifier "[" Expression . ","
						r = &FieldDecl{EmbeddedField: &EmbeddedField{TypeName: &TypeName{Name: &QualifiedIdent{Ident: id}, TypeArgs: &TypeArgs{LBracket: lbracket, TypeList: p.typeList2(x), RBracket: p.must(']')}}}}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				}
			default:
				//  identifier "[" . Expression
				r = &FieldDecl{IdentifierList: p.identifierList2(id), Type: &ArrayType{LBracket: lbracket, ArrayLength: p.expression(nil), RBracket: p.must(']'), ElementType: p.type1()}}
			}
		default:
			r = &FieldDecl{IdentifierList: p.identifierList2(id)}
			switch p.ch() {
			//                      Type
			case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
				r.Type = p.type1()
			//                       Tag
			case STRING_LIT:
				// ok
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		}
	case '*':
		r = &FieldDecl{EmbeddedField: &EmbeddedField{Star: p.shift(), TypeName: p.typeName()}}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	if p.ch() == STRING_LIT {
		r.Tag = p.shift()
	}
	r.Semicolon = p.semi(true)
	return r
}

func (p *parser) interfaceElems() (r []Node) {
	for {
		switch p.ch() {
		// InterfaceElem = MethodElem | TypeElem .
		//                MethodElem case IDENTIFIER:
		//                  TypeElem case '(', '*', '[', '~', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		case IDENTIFIER:
			id := p.shift()
			// identifier .
			switch p.ch() {
			case '(':
				// identifier . "("
				r = append(r, &MethodElem{MethodName: id, Signature: p.signature(), Semicolon: p.semi(true)})
			case '|':
				// identifier . "|"
				r = append(r, p.typeElem2(&TypeTerm{Type: id, Pipe: p.shift()}, true))
			case ';', '}':
				// identifier . ";"
				r = append(r, p.typeElem2(&TypeTerm{Type: id}, true))
			case '.':
				// identifier . "."
				r = append(r, p.typeElem2(&TypeTerm{Type: p.typeName2(&TypeName{Name: &QualifiedIdent{PackageName: id, Dot: p.shift(), Ident: p.must(IDENTIFIER)}})}, true))
			case '[':
				r = append(r, p.typeElem2(&TypeTerm{Type: p.typeName2(&TypeName{Name: &QualifiedIdent{Ident: id}})}, true))
			default:
				p.err(errorf("TODO %v", p.ch().str()))
				p.shift()
				return r
			}
		case '(', '*', '[', '~', ARROW, CHAN, FUNC, INTERFACE, MAP, STRUCT:
			r = append(r, p.typeElem(true))
		default:
			return r
		}
	}
}

// TypeElem = TypeTerm TypeElem_1 .
// TypeElem_1 =
// 	| TypeElem_1 "|" TypeTerm .
func (p *parser) typeElem(semi bool) (r *TypeElem) {
	r = &TypeElem{}
	for {
		switch p.ch() {
		//                  TypeTerm
		case '(', '*', '[', '~', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			n := p.typeTerm()
			r.TypeTerms = append(r.TypeTerms, n)
			switch p.ch() {
			case '|':
				n.Pipe = p.shift()
			}
		default:
			r.Semicolon = p.semi(semi)
			return r
		}
	}
}

func (p *parser) typeElem2(typeTerm *TypeTerm, semi bool) (r *TypeElem) {
	r = &TypeElem{TypeTerms: []*TypeTerm{typeTerm}}
	for {
		switch p.ch() {
		//                  TypeTerm
		case '(', '*', '[', '~', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			n := p.typeTerm()
			r.TypeTerms = append(r.TypeTerms, n)
			switch p.ch() {
			case '|':
				n.Pipe = p.shift()
			}
		default:
			r.Semicolon = p.semi(semi)
			return r
		}
	}
}

// TypeTerm = Type | UnderlyingType .
func (p *parser) typeTerm() (r *TypeTerm) {
	switch p.ch() {
	//                      Type
	case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
		return &TypeTerm{Type: p.type1()}
	//            UnderlyingType
	case '~':
		return &TypeTerm{Tilde: p.shift(), Type: p.type1()}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// TypeName = QualifiedIdent TypeName_1
// 	| identifier TypeName_2 .
// TypeName_1 =
// 	| TypeArgs .
// TypeName_2 =
// 	| TypeArgs .
func (p *parser) typeName() (r *TypeName) {
	switch p.ch() {
	case IDENTIFIER:
		id := p.shift()
		switch p.ch() {
		case '.':
			r = &TypeName{Name: &QualifiedIdent{PackageName: id, Dot: p.shift(), Ident: p.must(IDENTIFIER)}}
		default:
			r = &TypeName{Name: &QualifiedIdent{Ident: id}}
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	return p.typeName2(r)
}

func (p *parser) typeName2(in *TypeName) (r *TypeName) {
	r = in
	switch p.ch() {
	//                  TypeArgs
	case '[':
		r.TypeArgs = p.typeArgs()
		return r
	default:
		return r
	}
}

// TypeArgs = "[" TypeList TypeArgs_1 "]" .
// TypeArgs_1 =
// 	| "," .
func (p *parser) typeArgs() (r *TypeArgs) {
	return &TypeArgs{LBracket: p.must('['), TypeList: p.typeList(), Comma: p.opt(','), RBracket: p.must(']')}
}

func (p *parser) typeArgs2(lbracket Token, typ Node) (r *TypeArgs) {
	return &TypeArgs{LBracket: lbracket, TypeList: p.typeList2(typ), Comma: p.opt(','), RBracket: p.must(']')}
}

// ---------------------------------------------------------------- Expressions

// ExpressionList = Expression ExpressionList_1 .
// ExpressionList_1 =
// 	| ExpressionList_1 "," Expression .
func (p *parser) expressionList() (r []*ExpressionListItem) {
	for {
		switch p.ch() {
		//                Expression
		case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			n := &ExpressionListItem{Expression: p.expression(nil)}
			r = append(r, n)
			switch p.ch() {
			case ',':
				n.Comma = p.shift()
			default:
				return r
			}
		default:
			return r
		}
	}
}

// Expression = LogicalAndExpression Expression_1 .
// Expression_1 =
// 	| Expression_1 "||" LogicalAndExpression .
func (p *parser) expression(expr Node) (r Node) {
	r = p.logicalAndExpression(expr)
	for p.ch() == LOR {
		r = &BinaryExpression{A: r, Op: p.shift(), B: p.logicalAndExpression(nil)}
	}
	return r
}

// LogicalAndExpression = RelationalExpression LogicalAndExpression_1 .
// LogicalAndExpression_1 =
// 	| LogicalAndExpression_1 "&&" RelationalExpression .
func (p *parser) logicalAndExpression(expr Node) (r Node) {
	r = p.relationalExpression(expr)
	for p.ch() == LAND {
		r = &BinaryExpression{A: r, Op: p.shift(), B: p.relationalExpression(nil)}
	}
	return r
}

// RelationalExpression = AdditiveExpression RelationalExpression_1 .
// RelationalExpression_1 =
// 	| RelationalExpression_1 rel_op AdditiveExpression .
func (p *parser) relationalExpression(expr Node) (r Node) {
	r = p.additiveExpression(expr)
	for {
		switch p.ch() {
		//                    rel_op
		case '<', '>', EQ, GE, LE, NE:
			r = &BinaryExpression{A: r, Op: p.shift(), B: p.additiveExpression(nil)}
		default:
			return r
		}
	}
}

// AdditiveExpression = MultiplicativeExpression AdditiveExpression_1 .
// AdditiveExpression_1 =
// 	| AdditiveExpression_1 add_op MultiplicativeExpression .
func (p *parser) additiveExpression(expr Node) (r Node) {
	r = p.multiplicativeExpression(expr)
	for {
		switch p.ch() {
		//                    add_op
		case '+', '-', '^', '|':
			r = &BinaryExpression{A: r, Op: p.shift(), B: p.multiplicativeExpression(nil)}
		default:
			return r
		}
	}
}

// MultiplicativeExpression = UnaryExpr MultiplicativeExpression_1 .
// MultiplicativeExpression_1 =
// 	| MultiplicativeExpression_1 mul_op UnaryExpr .
func (p *parser) multiplicativeExpression(expr Node) (r Node) {
	r = expr
	if expr == nil {
		r = p.unaryExpression()
	}
	for {
		switch p.ch() {
		//                    mul_op
		case '%', '&', '*', '/', AND_NOT, SHL, SHR:
			r = &BinaryExpression{A: r, Op: p.shift(), B: p.unaryExpression()}
		default:
			return r
		}
	}
}

// UnaryExpr = PrimaryExpr
// 	| unary_op UnaryExpr .
func (p *parser) unaryExpression() (r Node) {
	switch p.ch() {
	//               PrimaryExpr case '(', '*', '[', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	case '(', '[', CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		return p.primaryExpression()
	//                  unary_op case '!', '&', '*', '+', '-', '^', ARROW:
	case '!', '&', '*', '+', '-', '^':
		return &UnaryExpr{UnaryOp: p.shift(), UnaryExpr: p.unaryExpression()}
	case ARROW:
		arrow := p.shift()
		// "<-" .
		switch p.ch() {
		case CHAN:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		default:
			return &UnaryExpr{UnaryOp: arrow, UnaryExpr: p.unaryExpression()}
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// PrimaryExpr = Operand PrimaryExpr_1
// 	| Conversion PrimaryExpr_2
// 	| MethodExpr PrimaryExpr_3 .
// PrimaryExpr_1 =
// 	| PrimaryExpr_1 PrimaryExpr_1_1 .
// PrimaryExpr_1_1 = Arguments
// 	| Index
// 	| Selector
// 	| Slice
// 	| TypeAssertion .
// PrimaryExpr_2 =
// 	| PrimaryExpr_2 PrimaryExpr_2_1 .
// PrimaryExpr_2_1 = Arguments
// 	| Index
// 	| Selector
// 	| Slice
// 	| TypeAssertion .
// PrimaryExpr_3 =
// 	| PrimaryExpr_3 PrimaryExpr_3_1 .
// PrimaryExpr_3_1 = Arguments
// 	| Index
// 	| Selector
// 	| Slice
// 	| TypeAssertion .
func (p *parser) primaryExpression() (r Node) {
	checkForLiteral := false
	switch p.ch() {
	//                   Operand case '(', '*', '[', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	case FLOAT_LIT, IMAG_LIT, INT_LIT, RUNE_LIT, STRING_LIT:
		r = p.shift()
	//                Conversion case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
	//                MethodExpr case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
	case '(':
		lparen := p.shift()
		switch x := p.exprOrType().(type) {
		case typ:
			r = &ParenType{LParen: lparen, Type: x, RParen: p.must(')')}
		default:
			r = &ParenExpr{LParen: lparen, Expression: x, RParen: p.must(')')}
		}
	case '*':
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	case ARROW:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	case '[', CHAN, INTERFACE, MAP, STRUCT:
		t := p.type1()
		switch p.ch() {
		case '{', body:
			r = &CompositeLit{LiteralType: t, LiteralValue: p.literalValue1()}
		case '(':
			r = &Conversion{Type: t, LParen: p.shift(), Expression: p.expression(nil), Comma: p.opt(','), RParen: p.must(')')}
		case '.':
			r = t
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case FUNC:
		f := p.shift()
		sig := p.signature()
		switch p.ch() {
		case '{', body:
			var lbr bool
			r = &FunctionLit{Func: f, Signature: sig, FunctionBody: &Block{LBrace: p.lbrace(&lbr), StatementList: p.statementList(), RBrace: p.fixlbr(lbr)}}
		case '(':
			r = &FunctionType{Func: f, Signature: sig}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case IDENTIFIER:
		checkForLiteral = true
		id := p.shift()
		//  identifier .
		switch p.ch() {
		case '.':
			dot := p.shift()
			//  identifier "." .
			switch p.ch() {
			case IDENTIFIER:
				//  identifier "." identifier
				r = &QualifiedIdent{PackageName: id, Dot: dot, Ident: p.shift()}
			case '(':
				lparen := p.shift()
				//  identifier "." "(" .
				switch p.ch() {
				case TYPE:
					//  identifier "." "(" . "type"
					return &TypeSwitchGuard{PrimaryExpr: id, Dot: dot, LParen: lparen, Type: p.shift(), RParen: p.must(')')}
				default:
					//  identifier "." "(" . identifier
					return p.primaryExpression2(&TypeAssertion{PrimaryExpr: id, Dot: dot, LParen: lparen, Type: p.type1(), RParen: p.must(')')}, false)
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			r = id
		}

		// QualifiedIdent .
		switch p.ch() {
		case '[':
			lbracket := p.shift()
			// QualifiedIdent "[" .
			switch p.ch() {
			//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
				switch x := p.exprOrType().(type) {
				case typ:
					r = &GenericOperand{OperandName: r, TypeArgs: p.typeArgs2(lbracket, x)}
				default:
					// QualifiedIdent "[" Expression .
					switch p.ch() {
					case ']':
						r = &Index{PrimaryExpr: r, LBracket: lbracket, Expression: x, RBracket: p.shift()}
					case ':':
						r = p.slice2(r, lbracket, x)
					case ',':
						r = &GenericOperand{OperandName: r, TypeArgs: p.typeArgs2(lbracket, x)}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				}
			case ':':
				r = p.slice2(r, lbracket, nil)
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
	return p.primaryExpression2(r, checkForLiteral)
}

func (p *parser) primaryExpression2(pe Node, checkForLiteral bool) (r Node) {
	if checkForLiteral {
		switch p.ch() {
		case '[':
			pe = p.indexOrSlice(pe)
			if _, ok := pe.(*Index); ok && p.ch() == '{' {
				pe = &CompositeLit{LiteralType: pe, LiteralValue: p.literalValue2()}
			}
		case '{':
			pe = &CompositeLit{LiteralType: pe, LiteralValue: p.literalValue2()}
		}
	}
	r = pe
	for {
		switch p.ch() {
		//                 Arguments
		case '(':
			r = p.arguments(r)
			//                     Index case '[':
			//                     Slice case '[':
		case '[':
			r = p.indexOrSlice(r)
			//                  Selector case '.':
			//             TypeAssertion case '.':
		case '.':
			dot := p.shift()
			switch p.ch() {
			case IDENTIFIER:
				r = &Selector{PrimaryExpr: r, Dot: dot, Ident: p.shift()}
			case '(':
				lparen := p.shift()
				switch p.ch() {
				case TYPE:
					return &TypeSwitchGuard{PrimaryExpr: r, Dot: dot, LParen: lparen, Type: p.shift(), RParen: p.must(')')}
				default:
					r = &TypeAssertion{PrimaryExpr: r, Dot: dot, LParen: lparen, Type: p.type1(), RParen: p.must(')')}
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			return r
		}
	}
}

func (p *parser) indexOrSlice(pe Node) (r Node) {
	if p.ch() != '[' {
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}

	lbracket := p.shift()
	// "[" .
	var expr Node
	switch p.ch() {
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		expr = p.expression(nil)
	}
	if p.ch() == ']' {
		// "[" Expression . "]"
		return &Index{PrimaryExpr: pe, LBracket: lbracket, Expression: expr, RBracket: p.shift()}
	}
	return p.slice2(pe, lbracket, expr)
}

// PrimaryExpr "[" Expression . ":"
func (p *parser) slice2(pe Node, lbracket Token, expr Node) (r *Slice) {
	var expr2 Node
	colon := p.must(':')
	switch p.ch() {
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		expr2 = p.expression(nil)
	}
	if p.ch() == ']' {
		// "[" Expression ":" [ Expression ] . "]"
		return &Slice{PrimaryExpr: pe, LBracket: lbracket, Expression: expr, Colon: colon, Expression2: expr2, RBracket: p.shift()}
	}

	return &Slice{PrimaryExpr: pe, LBracket: lbracket, Expression: expr, Colon: colon, Expression2: expr2, Colon2: p.must(':'), Expression3: p.expression(nil), RBracket: p.shift()}
}

// Arguments = "(" ")"
// 	| "(" ExpressionList Arguments_1 Arguments_2 ")"
// 	| "(" Type "," ExpressionList Arguments_3 Arguments_4 ")"
// 	| "(" Type Arguments_5 Arguments_6 ")" .
// Arguments_1 =
// 	| "..." .
// Arguments_2 =
// 	| "," .
// Arguments_3 =
// 	| "..." .
// Arguments_4 =
// 	| "," .
// Arguments_5 =
// 	| "..." .
// Arguments_6 =
// 	| "," .
func (p *parser) arguments(pe Node) (r *Arguments) {
	r = &Arguments{PrimaryExpr: pe, LParen: p.must('(')}
	if p.ch() == ')' {
		r.RParen = p.shift()
		return r
	}

	switch x := p.exprOrType().(type) {
	case typ:
		r.Type = x
		switch p.ch() {
		case ',':
			r.Comma = p.shift()
			r.ExpressionList = p.expressionList()
		case ')':
			// ok
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	default:
		switch p.ch() {
		case ')':
			r.ExpressionList = []*ExpressionListItem{{Expression: x}}
			r.RParen = p.shift()
			return r
		case ',':
			comma := p.shift()
			switch p.ch() {
			case ')':
				r.ExpressionList = []*ExpressionListItem{{Expression: x, Comma: comma}}
			default:
				r.ExpressionList = append([]*ExpressionListItem{{Expression: x, Comma: comma}}, p.expressionList()...)
			}
		case ELLIPSIS:
			r.ExpressionList = []*ExpressionListItem{{Expression: x}}
			r.Ellipsis = p.shift()
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	}
	if p.ch() == ELLIPSIS {
		r.Ellipsis = p.shift()
	}
	if p.ch() == ',' {
		r.Comma2 = p.shift()
	}
	r.RParen = p.must(')')
	return r
}

func (p *parser) exprOrType() (r Node) {
	switch p.ch() {
	//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
	case '!', '&', '+', '-', '^', FLOAT_LIT, IMAG_LIT, INT_LIT, RUNE_LIT, STRING_LIT:
		return p.expression(nil)
	//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
	case '(':
		lparen := p.shift()
		// "(" .
		switch x := p.exprOrType().(type) {
		case typ:
			// "(" Type .
			switch p.ch() {
			case ')':
				r = &ParenType{LParen: lparen, Type: x, RParen: p.shift()}
				switch p.ch() {
				case '(':
					return p.expression(p.primaryExpression2(r, false))
				default:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			// "(" Expression .
			return p.expression(p.primaryExpression2(&ParenExpr{LParen: lparen, Expression: x, RParen: p.must(')')}, false))
		}
	case '*':
		star := p.shift()
		// "*" .
		switch x := p.exprOrType().(type) {
		case typ:
			return &PointerType{Star: star, BaseType: x}
		default:
			return &UnaryExpr{UnaryOp: star, UnaryExpr: x}
		}
	case ARROW:
		arrow := p.shift()
		switch p.ch() {
		case CHAN:
			// ChannelType = "<-" "chan" ElementType
			// 	| "chan" "<-" ElementType
			// 	| "chan" ElementType .
			r = &ChannelType{ArrowPre: arrow, Chan: p.shift(), ElementType: p.type1()}
			switch p.ch() {
			case ')':
				return r
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			return p.expression(&UnaryExpr{UnaryOp: arrow, UnaryExpr: p.unaryExpression()})
		}
	case '[', CHAN, INTERFACE, MAP, STRUCT:
		t := p.type1()
		switch p.ch() {
		case '{', body:
			return p.expression(p.primaryExpression2(&CompositeLit{LiteralType: t, LiteralValue: p.literalValue1()}, false))
		case '(':
			return p.expression(p.primaryExpression2(&Conversion{Type: t, LParen: p.shift(), Expression: p.expression(nil), Comma: p.opt(','), RParen: p.must(')')}, false))
		case ',', ')', ']':
			return t
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case FUNC:
		f := p.shift()
		sig := p.signature()
		switch p.ch() {
		case '{', body:
			var lbr bool
			return p.expression(p.primaryExpression2(&FunctionLit{Func: f, Signature: sig, FunctionBody: &Block{LBrace: p.lbrace(&lbr), StatementList: p.statementList(), RBrace: p.fixlbr(lbr)}}, false))
		case ')', ']':
			return &FunctionType{Func: f, Signature: sig}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	case IDENTIFIER:
		id := p.shift()
		//  identifier .
		switch p.ch() {
		case '.':
			dot := p.shift()
			//  identifier "." .
			switch p.ch() {
			case IDENTIFIER:
				//  identifier "." identifier
				r = &QualifiedIdent{PackageName: id, Dot: dot, Ident: p.shift()}
			case '(':
				lparen := p.shift()
				//  identifier "." "(" .
				switch p.ch() {
				case TYPE:
					return &TypeSwitchGuard{PrimaryExpr: id, Dot: dot, LParen: lparen, Type: p.shift(), RParen: p.must(')')}
				default:
					//  identifier "." "(" . identifier
					return p.expression(p.primaryExpression2(&TypeAssertion{PrimaryExpr: id, Dot: dot, LParen: lparen, Type: p.type1(), RParen: p.must(')')}, false))
				}
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			r = id
		}
		// QualifiedIdent .
		switch p.ch() {
		case '[':
			lbracket := p.shift()
			// QualifiedIdent "[" .
			switch p.ch() {
			//                Expression case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
			//                      Type case '(', '*', '[', ARROW, CHAN, FUNC, IDENTIFIER, INTERFACE, MAP, STRUCT:
			case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
				switch x := p.exprOrType().(type) {
				case typ:
					p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
					p.shift()
					return r
				default:
					_ = lbracket
					_ = x
					// QualifiedIdent "[" Expression .
					switch p.ch() {
					case ']':
						return p.expression(p.primaryExpression2(&Index{PrimaryExpr: r, LBracket: lbracket, Expression: x, RBracket: p.shift()}, true))
					case ':':
						return p.expression(p.primaryExpression2(p.slice2(r, lbracket, x), false))
					case ',':
						typeArgs := p.typeArgs2(lbracket, x)
						var tn *TypeName
						switch y := r.(type) {
						case *QualifiedIdent:
							tn = &TypeName{Name: y, TypeArgs: typeArgs}
						case Token:
							tn = &TypeName{Name: &QualifiedIdent{Ident: y}, TypeArgs: typeArgs}
						default:
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}

						switch p.ch() {
						case ')', ']', ',':
							return tn
						case '{':
							return p.expression(p.primaryExpression2(&GenericOperand{OperandName: r, TypeArgs: typeArgs}, true))
						case '(':
							return p.expression(p.primaryExpression2(&GenericOperand{OperandName: r, TypeArgs: typeArgs}, false))
						default:
							p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
							p.shift()
							return r
						}
					default:
						p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
						p.shift()
						return r
					}
				}
			case ':':
				return p.expression(p.primaryExpression2(p.slice2(r, lbracket, nil), false))
			default:
				p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
				p.shift()
				return r
			}
		default:
			return p.expression(p.primaryExpression2(r, true))
		}
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}

// LiteralValue1 = lbrace ElementList LiteralValue1_1 "#fixlbr" "}"
// 	| lbrace "#fixlbr" "}" .
// LiteralValue1_1 =
// 	| "," .
func (p *parser) literalValue1() (r *LiteralValue) {
	var lbr bool
	return &LiteralValue{LBrace: p.lbrace(&lbr), ElementList: p.elementList(), RBrace: p.fixlbr(lbr)}
}

// LiteralValue2 = "{" "}"
// 	| "{" ElementList LiteralValue2_1 "}" .
// LiteralValue2_1 =
// 	| "," .
func (p *parser) literalValue2() (r *LiteralValue) {
	return &LiteralValue{LBrace: p.must('{'), ElementList: p.elementList(), RBrace: p.must('}')}
}

// ElementList = KeyedElement ElementList_1 .
// ElementList_1 =
// 	| ElementList_1 "," KeyedElement .
func (p *parser) elementList() (r []*KeyedElement) {
	for p.ch() != '}' {
		switch p.ch() {
		//              KeyedElement
		case '!', '&', '(', '*', '+', '-', '[', '^', '{', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT, body:
			n := p.keyedElement()
			r = append(r, n)
			if p.ch() == ',' {
				n.Comma = p.shift()
			}
		default:
			p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
			p.shift()
			return r
		}
	}
	return r
}

// KeyedElement = Key ":" Element
// 	| Element .
func (p *parser) keyedElement() (r *KeyedElement) {
	ke := p.keyOrElement()
	switch p.ch() {
	case ':':
		return &KeyedElement{Key: ke, Colon: p.shift(), Element: p.keyedElement()}
	default:
		return &KeyedElement{Element: ke}
	}
}

// Key = Expression | LiteralValue1 .
// Element = Expression | LiteralValue1 .
func (p *parser) keyOrElement() (r Node) {
	switch p.ch() {
	//                Expression
	case '!', '&', '(', '*', '+', '-', '[', '^', ARROW, CHAN, FLOAT_LIT, FUNC, IDENTIFIER, IMAG_LIT, INTERFACE, INT_LIT, MAP, RUNE_LIT, STRING_LIT, STRUCT:
		return p.expression(nil)
	//             LiteralValue1
	case '{', body:
		return p.literalValue1()
	default:
		p.err(errorf("TODO %v", p.s.Tok.Ch.str()))
		p.shift()
		return r
	}
}
