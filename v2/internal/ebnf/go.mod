module modernc.org/gc/v2/internal/ebnf

go 1.18

require (
	github.com/dustin/go-humanize v1.0.0
	modernc.org/ebnf v1.1.0
	modernc.org/ebnfutil v1.0.2
	modernc.org/gc/v2 v2.0.0-20220516112824-550e07b9c9dd
)

require (
	modernc.org/strutil v1.1.2 // indirect
	modernc.org/token v1.0.0 // indirect
)
