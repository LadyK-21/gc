module modernc.org/gc/v2

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/pmezard/go-difflib v1.0.0
	modernc.org/scannertest v1.0.0
	modernc.org/token v1.0.0
)
